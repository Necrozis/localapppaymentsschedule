var mongoose = require('mongoose');

var SupplierSchema = new mongoose.Schema({
  name: {type: String, required: true},
  debt: {type: Number, default: 0},
  credit: {type: Number, default: 0},
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Supplier', SupplierSchema);