var mongoose = require('mongoose');

var PaymentSchema = new mongoose.Schema({
  author: {type: String, required: true},
  invoice: {type: String, required: true},
  info: {type: String},
  date: {type: Date},
  summ: {type: Number, required: true},
  ready: {type: Boolean, default: false},
  soon: {type: Boolean},
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Payment', PaymentSchema);