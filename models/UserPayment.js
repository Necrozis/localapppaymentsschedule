var mongoose = require('mongoose');

var UserPaymentSchema = new mongoose.Schema({
  debt: {type: String, required: true},
  date: {type: Date, required: true},
  summ: {type: Number, required: true},
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('UserPayment', UserPaymentSchema);
