var mongoose = require('mongoose');

var InvoiceSchema = new mongoose.Schema({
  author: {type: String, required: true},
  number: {type: String, required: true},
  supplier: {type: String, required: true},
  date: {type: Date, required: true},
  summ: {type: Number, required: true},
  info: {type: String},
  paid: {type: Boolean, default: false},
  delivered: {type: Boolean, default: false},
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Invoice', InvoiceSchema);
