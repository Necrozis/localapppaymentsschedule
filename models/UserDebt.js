var mongoose = require('mongoose');

var UserDebtSchema = new mongoose.Schema({
  user: {type: String, required: true},
  date: {type: Date, required: true},
  debt: {type: Number, required: true},
  closed: {type: Boolean, default: false},
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('UserDebt', UserDebtSchema);
