var socket = io.connect('http://192.168.1.111:27018');
var userStorage = localStorage;
var userActiveId = '';
var connected = false;

function curDateString(){
  return moment().format('DD.MM.YYYY');
};

function getDate(date){
  return moment(date, 'DD.MM.YYYY').toDate();
};

function disableAllButtons(status){
  $(":button").each(function(i){
    $(this).prop('disabled', status);
  });
};

function modalFormReset(modal){
  $('#' + modal)
    .find(':radio, :checkbox').removeAttr('checked').end()
    .find('textarea, :text, select').val('')
};

disableAllButtons(true);

socket.on('connect', function () {
  connected = true;
  disableAllButtons(false);
  resultNotify('Успешное подключение с серверу','success');
});

socket.on('disconnect', function () {
  connected = false;
  disableAllButtons(true);
  resultNotify('Связь с сервером разорвана','warning');
});

socket.on('connect_failed', function(){
  disableAllButtons(true);
  resultNotify('Невозможно подключиться к серверу','warning');
});

socket.on('register-result',function(result){
  var resultText = JSON.stringify(result);
});

socket.on('login-result',function(result){
  if (result.err) {
    resultNotify(JSON.stringify(result.cause),'danger');
  } else {
    if (result.exist == true && result.access.paymentgraph == true) {
      userStorage.clear();
      userStorage.setItem('user', JSON.stringify(result.user));
      userActiveId = result.user['_id'];
      resultNotify('Вы авторизовались как ' + result.user.fullname,'success');
      $('#login-form').fadeOut('slow');
      $('#bot-panel').show();
      $('#bot-panel').animate({ bottom: '0px', opacity: 1 }, 'slow', 'easeOutSine');
      socket.emit('update-all');
    } else if(result.exist == true && result.access.paymentgraph == false) {
      resultNotify('Недостаточно прав доступа','danger');
    } else {
      resultNotify('Неверные данные для авторизации','danger');
    };
  };
});

socket.on('payment-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка добавления платежа','danger');
  } else {
    if (result.created) {
      $('#payment-modal').modal('hide');
      resultNotify('Платеж на инвойс ' + result.payment.invoice + ' успешно добавлен','success');
      socket.emit('payment-getAll');
      socket.emit('invoice-getAll');
      modalFormReset('payment-modal');
    } else {
      resultNotify('Ошибка добавления платежа','danger');
    };
  };
});

socket.on('payment-confirm-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка подтверждения платежа','danger');
  } else if(result.err == false) {
    resultNotify('Платеж подтвержден успешно','success');
    socket.emit('payment-getAll');
    socket.emit('invoice-getAll');
  };
});

socket.on('payment-delete-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка удаления платежа','danger');
  } else if(result.err == false) {
    resultNotify('Платеж удален успешно','success');
    socket.emit('payment-getAll');
    socket.emit('invoice-getAll');
  };
});

socket.on('payment-list',function(result){
  $('#payment-list').empty();
  userStorage.setItem('payments', JSON.stringify(result.payments));
  result.payments.forEach(function(payment) {
    if(payment.soon == true){
      var paymentCard = $(paymentCardElement(payment['_id'], payment.invoice, payment.soon, payment.summ, payment.info));
      $('#payment-list').append(paymentCard);
    };
  });
  $("#payment-panel").fadeIn('slow');
});

socket.on('supplier-list',function(result){
  $('#invoice-supplier').empty();
  $('#supplier-list').empty();
  $('#invoice-supplier').append($('<option value="" disabled selected>Выбрать поставщика...</option>'));
  userStorage.setItem('suppliers', JSON.stringify(result.suppliers));
  result.suppliers.forEach(function(supplier) {
    $('#invoice-supplier').append($('<option></option>').attr('value',supplier['_id']).text(supplier.name));
    var supplierCard = supplierCardElement(supplier['_id'], supplier.name, supplier.debt, supplier.credit);
    $('#supplier-list').append(supplierCard);
  });
  $("#supplier-panel").fadeIn('slow');
});

socket.on('supplier-result',function(result){
  if (result.err) {
    resultNotify(JSON.stringify(result.cause),'danger');
  } else {
    if (result.created) {
      $('#supplier-modal').modal('hide');
      resultNotify('Поставщик ' + result.supplier.name + ' успешно добавлен','success');
      socket.emit('supplier-getAll');
      modalFormReset('supplier-modal');
    } else {
      resultNotify('Ошибка добавления поставщика','danger');
    };
  };
});

socket.on('supplier-delete-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка удаления поставщика','danger');
  } else if(result.err == false) {
    resultNotify('Поставщик удален успешно','success');
    socket.emit('supplier-getAll');
  };
});

socket.on('supplier-change-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка изменения долгов поставщика','danger');
  } else if(result.err == false) {
    resultNotify('Долг успешно обновлен','success');
    socket.emit('supplier-getAll');
  };
});

socket.on('invoice-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка добавления инвойса','danger');
  } else {
    if (result.created) {
      $('#invoice-modal').modal('hide');
      resultNotify('Инвойс ' + result.invoice.number + ' успешно добавлен','success');
      socket.emit('invoice-getAll');
      modalFormReset('invoice-modal');
    } else {
      resultNotify('Ошибка добавления инвойса','danger');
    };
  };
});

socket.on('invoice-list',function(result){
  $('#payment-invoice').empty();
  $('#invoice-list').empty();
  $('#payment-invoice').append($('<option value="" disabled selected>Выбрать инвойс...</option>'));
  userStorage.setItem('invoices', JSON.stringify(result.invoices));
  result.invoices.forEach(function(invoice) {
    if (invoice.paid == false){
      $('#payment-invoice').append($('<option></option>').attr('value',invoice['_id']).text(invoice.number));
    };
  });
  invoicesRender();
  $("#invoice-panel").fadeIn('slow');
});

socket.on('invoice-confirm-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка подтверждения инвойса','danger');
  } else if(result.err == false) {
    resultNotify('Инвойс подтвержден успешно','success');
    socket.emit('payment-getAll');
    socket.emit('invoice-getAll');
  };
});

socket.on('invoice-delete-result',function(result){
  if (result.err == true) {
    resultNotify(JSON.stringify(result.cause),'danger');
    resultNotify('Ошибка удаления инвойса','danger');
  } else if(result.err == false) {
    resultNotify('Инвойс удален успешно','success');
    socket.emit('payment-getAll');
    socket.emit('invoice-getAll');
  };
});

$('#update-all').click(function() {
  socket.emit('update-all');
  modalFormReset('supplier-modal');
  modalFormReset('invoice-modal');
  modalFormReset('payment-modal');
});

$('#register-submit').click(function() {
  var data = {};
  data.username = $('#register-username').val();
  data.fullname = $('#register-fullname').val();
  data.password = $('#register-password').val();
  socket.emit('register-submit',data);
});

$('#login-submit').click(function() {
  var data = {};
  data.username = $('#login-username').val();
  data.password = $('#login-password').val();
  socket.emit('login-submit',data);
});

$('#supplier-submit').click(function() {
  var data = {};
  data.name = $('#supplier-name').val();
  socket.emit('supplier-submit',data);
});

$('#invoice-submit').click(function() {
  var data = {};
  data.author = userActiveId;
  data.info = $('#invoice-info').val();
  data.date = getDate($('#invoice-date').val());
  data.summ = $('#invoice-summ').val();
  data.number = $('#invoice-number').val();
  data.supplier = $("#invoice-supplier option:selected").text();
  socket.emit('invoice-submit',data);
});

$('#payment-submit').click(function() {
  var data = {};
  data.author = userActiveId;
  data.info = $('#payment-info').val();
  data.date = getDate($('#payment-date').val());
  data.summ = $('#payment-summ').val();
  data.soon = $('#payment-soon').is(':checked');
  data.ready = $('#payment-ready').is(':checked');
  data.invoice = $("#payment-invoice option:selected").text();
  socket.emit('payment-submit',data);
});

function paymentCardElement(id,inv_num,soon,summ,info){
  if (soon == true){
    var status = "Скорая оплата";
    var status_style = "label-warning";
  } else {
    var status = "Не проведен";
    var status_style = "label-danger";
  }
  var elem =
    '<div class="col-md-2">' +
      '<div class="card card-payment">' +
        '<h5><span style="font-weight:bold;">' + inv_num + '</span><span class="label ' + status_style + '">' + status +'</span></h5>' +
        '<hr style="margin: 8px 0">' +
        '<div class="well">' + info + '</div>' +
        '<span style="text-align:center;color:green;font-weight:bold;font-size:20px;">' + summ + '$' + '</span>' +
        '<hr style="margin: 8px 0">' +
        '<button data-id="' + id + '" class="btn btn-success btn-block" onclick="paymentConfirm($(this))" type="button">Провести</button>' +
        '<button data-id="' + id + '"  class="btn btn-danger btn-block" onclick="paymentDelete($(this))" type="button">Удалить</button>' +
      '</div>' +
    '</div>';
  return elem;
};

function invoiceCardElement(id,name,info,date,supplier,summ,rest,persent,payments){
  var elem =
  '<div class="col-md-6">' +
    '<div class="card card-invoice">' +
      '<h5><span style="font-weight:bold;">' + name + '</span><span class="label label-danger">' + info + '</span></h5>' +
      '<hr style="margin: 8px 0">' +
      '<div class="row">' +
        '<div class="col-md-2 invoice-cell">' + moment(date).format('DD.MM.YYYY') + '</div>' +
        '<div class="col-md-2 invoice-cell">' + supplier + '</div>' +
        '<div style="color:green;font-weight:bold;" class="col-md-2 invoice-cell">' + summ + "$" + '</div>' +
        '<div style="color:#ea811a;font-weight:bold;" class="col-md-2 invoice-cell">' + persent + "%" + '</div>' +
        '<div style="color:#d517f2;font-weight:bold;" class="col-md-2 invoice-cell">' + rest + "$" + '</div>' +
        '<div class="col-md-2 invoice-cell">' +
          '<a style="font-weight:bold" role="button" data-toggle="collapse" href="#' + id + '-payments" aria-expanded="false" aria-controls="' + id + '-payments">Оплаты</a>' +
        '</div>' +
      '</div>' +
      '<div id="' + id + '-payments" class="panel-body collapse">' +
        '<hr style="margin: 8px 0">' +
        invoicePayments(payments.reverse()) +
      '</div>' +
      '<hr style="margin: 8px 0">' +
      '<button data-id="' + id + '"  class="btn btn-success btn-block" onclick="invoiceConfirm($(this))" type="button">Подтвердить получение</button>' +
      '<button data-id="' + id + '"  class="btn btn-danger btn-block" onclick="invoiceDelete($(this))" type="button">Удалить</button>' +
    '</div>' +
  '</div>'
  return elem;
};

function invoiceRestCalculations(){
  var payments = JSON.parse(userStorage.getItem('payments'));
  var invoices = JSON.parse(userStorage.getItem('invoices'));
  var invoice_active = invoices.filter(function(payment) {
    return payment.delivered == false;
  });
  var tempArr = [];
  invoice_active.forEach(function(invoice, i, arr){
    var tempInvoice = invoice;
    var payments_filer = payments.filter(function(payment) {
      return payment.invoice == invoice.number;
    });
    var payments_ready = payments_filer.filter(function(payment) {
      return payment.ready == true;
    });
    tempInvoice.payments = payments_filer;
    var paymentsSumm = 0;
    payments_ready.forEach(function(payment){
      paymentsSumm += parseFloat(payment.summ);
    });

    var invoiceRest = parseFloat(invoice.summ) - parseFloat(paymentsSumm);
    tempInvoice.rest = invoiceRest.toFixed(2);
    var invoicePersent = (paymentsSumm*100)/parseFloat(invoice.summ);
    tempInvoice.persent = invoicePersent.toFixed(2);
    tempArr.push(tempInvoice);
  });
  userStorage.setItem('invoices_active', JSON.stringify(tempArr));
  return tempArr;
};

function invoicesRender(){
  var invoices = invoiceRestCalculations();
  invoices.forEach(function(invoice){
    var invoiceCard = invoiceCardElement(invoice['_id'], invoice.number, invoice.info, invoice.date, invoice.supplier, invoice.summ, invoice.rest, invoice.persent, invoice.payments);
    $('#invoice-list').append(invoiceCard);
  });
};

function invoicePayments(payments){
  if(payments){
    var elem = "";
    var arr = payments;
    arr.forEach(function(payment) {
      if(payment.date !== null){
        var date = moment(payment.date).format('DD.MM.YYYY');
        var wellColor = '#dfffcc';
      } else {
        var date = "Ожидается";
        var wellColor = '#e6ccff';
      };
      var row =
        '<div style="margin-bottom: 5px;background-color:' + wellColor + '" class="col-md-12 well">' +
          '<div style="text-align:left" class="col-md-2">' + date + '</div>' +
          '<div style="text-align:left;color:green;" class="col-md-2">' + payment.summ + "$" + '</div>' +
          '<div style="text-align:left" class="col-md-6">' + payment.info + '</div>' +
          '<div style="text-align:center" class="col-md-2">' +
            '<button style="margin-right:2px" data-id="' + payment._id + '" class="btn btn-success" onclick="paymentConfirm($(this))" type="button"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>' +
            '<button style="margin-left:2px" data-id="' + payment._id + '"  class="btn btn-danger" onclick="paymentDelete($(this))" type="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>' +
          '</div>' +
        '</div>';
      elem += row;
    });
    return elem;
  }else{
    return "";
  };
};

function supplierCardElement(id,name,debt,credit){
  var elem =
  '<div class="col-md-2">' +
    '<div class="card card-supplier">' +
      '<h5><span style="font-weight:bold;">' + name + '</span></h5>' +
      '<hr style="margin: 8px 0">' +
      '<p style="text-align: center;">Долг поставщика</p>' +
      '<div class="input-group">' +
        '<span class="input-group-btn">' +
          '<button id="debt-minus" data-id="' + id + '" class="btn btn-minus" onclick="supplierChange($(this))" type="button">-</button>' +
        '</span>' +
        '<input style="text-align: center;" type="text" class="form-control" value="' + debt + '" readonly>' +
        '<span class="input-group-btn">' +
          '<button id="debt-plus" data-id="' + id + '" class="btn btn-plus" onclick="supplierChange($(this))" type="button">+</button>' +
        '</span>' +
      '</div>' +
      '<hr style="margin: 4px 0">' +
      '<p style="text-align: center;">Долг компании</p>' +
      '<div class="input-group">' +
        '<span class="input-group-btn">' +
          '<button id="credit-minus" data-id="' + id + '" class="btn btn-minus" onclick="supplierChange($(this))" type="button">-</button>' +
        '</span>' +
        '<input style="text-align: center;" type="text" class="form-control" value="' + credit + '" readonly>' +
        '<span class="input-group-btn">' +
          '<button id="credit-plus" data-id="' + id + '" class="btn btn-plus" onclick="supplierChange($(this))" type="button">+</button>' +
        '</span>' +
      '</div>' +
      '<hr style="margin: 8px 0">' +
      '<button data-id="' + id + '" class="btn btn-danger btn-block" onclick="supplierDelete($(this))" type="button">Удалить</button>' +
    '</div>' +
  '</div>'
  return elem;
};

function supplierChange(element){
  var elemId = $(element).attr('id').split("-");
  var elemField = elemId[0];
  var elemOper = elemId[1];
  var data = {
    _id: $(element).data('id'),
    value: prompt("Введите сумму операции"),
    oper: elemOper,
    field: elemField
  };
  socket.emit('supplier-change', data);
};

function invoiceConfirm(element){
  var data = {
    _id: $(element).data('id')
  };
  socket.emit('invoice-confirm', data);
};

function invoiceDelete(element){
  var data = {
    _id: $(element).data('id')
  };
  socket.emit('invoice-delete', data);
};

function paymentConfirm(element){
  var data = {
    _id: $(element).data('id'),
    date: getDate(prompt("Введите дату оплаты", curDateString()))
  };
  socket.emit('payment-confirm', data);
};

function paymentDelete(element){
  var data = {
    _id: $(element).data('id')
  };
  socket.emit('payment-delete', data);
};

function supplierDelete(element){
  var data = {
    _id: $(element).data('id')
  };
  socket.emit('supplier-delete', data);
};

function EmptyAll(){
  $('#payment-invoice').empty();
  $('#invoice-supplier').empty();
  $('#invoice-list').empty();
  $('#supplier-list').empty();
  $('#payment-list').empty();
}

function resultNotify(msg,stl){
  $.notify({
    message: msg
  },{
    z_index: 9999,
    placement: {
      from: "top",
      align: "right"
    },
    type: stl,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    }
  });
};

$('.date').mask('00.00.0000');
moment.locale('ru');
