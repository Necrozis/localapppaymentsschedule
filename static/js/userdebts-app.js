var socket = io.connect('http://192.168.1.111:27018');
var userStorage = localStorage;
var userActiveId = '';
var connected = false;

function curDateString(){
  return moment().format('DD.MM.YYYY');
};

function getDate(date){
  return moment(date, 'DD.MM.YYYY').toDate();
};

function disableAllButtons(status){
  $(":button").each(function(i){
    $(this).prop('disabled', status);
  });
};

function modalFormReset(modal){
  $('#' + modal)
    .find(':radio, :checkbox').removeAttr('checked').end()
    .find('textarea, :text, select').val('')
};

disableAllButtons(true);

socket.on('connect', function () {
  connected = true;
  disableAllButtons(false);
  resultNotify('Успешное подключение с серверу','success');
});

socket.on('disconnect', function () {
  connected = false;
  disableAllButtons(true);
  resultNotify('Связь с сервером разорвана','warning');
});

socket.on('connect_failed', function(){
  disableAllButtons(true);
  resultNotify('Невозможно подключиться к серверу','warning');
});

socket.on('login-result',function(result){
  if (result.err) {
    resultNotify(JSON.stringify(result.cause),'danger');
  } else {
    if (result.exist == true && result.access.paymentgraph == true) {
      userStorage.clear();
      userStorage.setItem('user', JSON.stringify(result.user));
      userActiveId = result.user['_id'];
      resultNotify('Вы авторизовались как ' + result.user.fullname,'success');
      $('#login-form').fadeOut('slow');
      $('#bot-panel').show();
      $('#bot-panel').animate({ bottom: '0px', opacity: 1 }, 'slow', 'easeOutSine');
      socket.emit('update-all');
    } else if(result.exist == true && result.access.paymentgraph == false) {
      resultNotify('Недостаточно прав доступа','danger');
    } else {
      resultNotify('Неверные данные для авторизации','danger');
    };
  };
});

$('#login-submit').click(function() {
  var data = {};
  data.username = $('#login-username').val();
  data.password = $('#login-password').val();
  socket.emit('login-submit',data);
});

function resultNotify(msg,stl){
  $.notify({
    message: msg
  },{
    z_index: 9999,
    placement: {
      from: "top",
      align: "right"
    },
    type: stl,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    }
  });
};

$('.date').mask('00.00.0000');
moment.locale('ru');
