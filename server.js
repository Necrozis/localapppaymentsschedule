var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var options = require('./config.json');

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

var url = 'mongodb://' + options.u + ':' + options.p + '@localhost:27017/' + options.d + '?authMechanism=' + options.m + '&authSource=' + options.s;

var db = mongoose.connect(url, function(err) {
  if (err) {
		throw err
	} else {
		console.log('mongoose - ok');
	};
});

var User = require('./models/User');
var UserDebt = require('./models/UserDebt');
var UserPayment = require('./models/UserPayment');
var Supplier = require('./models/Supplier');
var Invoice = require('./models/Invoice');
var Payment = require('./models/Payment');

function userCreate(data, callback){
  var result = new Object();
  usernameCheck(data,function(check){
    if (check.exist){
      result.err = true;
      result.cause = 'User ' + data.username + ' already exist.';
      callback(result);
    };
    if (!check.exist){
      var user = new User();
      user.username = data.username;
      user.password = data.password;
      user.fullname = data.fullname;
      user.save(function(err) {
        if (err) {
          result.err = true;
          result.cause = err;
          result.created = false;
          callback(result);
        } else {
          result.err = false;
          result.created = true;
          result.user = data;
          callback(result);
        };
      });
    };
  });
};

function userDebtCreate(data, callback){
  var result = new Object();
  var userDebt = new UserDebt();
  userDebt.user = data.user;
  userDebt.date = data.date;
  userDebt.debt = data.debt;
  userDebt.save(function(err, data) {
    if (err) {
      result.err = true;
      result.cause = err;
      result.created = false;
      callback(result);
    } else {
      result.err = false;
      result.created = true;
      result.data = data;
      callback(result);
    };
  });
};

function userPaymentCreate(data, callback){
  var result = new Object();
  var userPayment = new UserPayment();
  userPayment.debt = data.debt;
  userPayment.date = data.date;
  userPayment.summ = data.summ;
  userPayment.save(function(err, data) {
    if (err) {
      result.err = true;
      result.cause = err;
      result.created = false;
      callback(result);
    } else {
      result.err = false;
      result.created = true;
      result.data = data;
      callback(result);
    };
  });
};

function usernameCheck(data,callback){
	User.findOne({'username': data.username}, function(err, user) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (user) {
      result.err = false;
      result.exist = true;
      result.user = user;
      callback(result);
    } else {
      result.err = false;
      result.exist = false;
      callback(result);
    }
  });
};

function loginCheck(data,callback){
	User.findOne({'username': data.username, 'password': data.password}, function(err, user) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (user) {
      result.err = false;
      result.exist = true;
      result.user = user;
      callback(result);
    } else {
      result.err = false;
      result.exist = false;
      callback(result);
    }
  });
};

function supplierCreate(data, callback){
  var result = new Object();
  supplierCheck(data,function(check){
    if (check.exist){
      result.err = true;
      result.cause = 'Supplier ' + data.name + ' already exist.';
      callback(result);
    };
    if (!check.exist){
      var supplier = new Supplier();
      supplier.name = data.name;
      supplier.save(function(err) {
        if (err) {
          result.err = true;
          result.cause = err;
          result.created = false;
          callback(result);
        } else {
          result.err = false;
          result.created = true;
          result.supplier = data;
          callback(result);
        };
      });
    };
  });
};

function supplierCheck(data, callback){
	Supplier.findOne({'name': data.name},function (err, supplier) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (supplier) {
      result.err = false;
      result.exist = true;
      result.supplier = supplier;
      callback(result);
    } else {
      result.exist = false;
      callback(result);
    }
  });
};

function supplierChange(data, callback){
  var result = new Object();
  Supplier.findById(data._id, function (err, doc) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      var value = parseInt(data.value);
      result.err = false;
      if(data.oper == "plus"){
        doc[data.field] += value;
      } else if(data.oper == "minus"){
        doc[data.field] -= value;
      };
      doc.save();
      callback(result);
    };
  });
};

function supplierDelete(data, callback){
  var result = new Object();
  Supplier.findByIdAndRemove(data._id, function (err) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      result.err = false;
      callback(result);
    };
  });
};

function supplierGetAll(callback){
	Supplier.find({},function (err, suppliers) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (suppliers) {
      result.err = false;
      result.exist = true;
      result.suppliers = suppliers;
      callback(result);
    } else {
      result.exist = false;
      callback(result);
    }
  });
};

function invoiceCreate(data, callback){
  var result = new Object();
  invoiceCheck(data,function(check){
    if (check.exist){
      result.err = true;
      result.cause = 'Invoice ' + data.number + ' already exist.';
      callback(result);
    };
    if (!check.exist){
      var invoice = new Invoice();
      invoice.author = data.author;
      invoice.number = data.number;
      invoice.supplier = data.supplier;
      invoice.date = data.date;
      invoice.summ = data.summ;
      invoice.info = data.info;
      invoice.rest = data.summ;
      invoice.save(function(err) {
        if (err) {
          result.err = true;
          result.cause = err;
          result.created = false;
          callback(result);
        } else {
          result.err = false;
          result.created = true;
          result.invoice = data;
          callback(result);
        };
      });
    };
  });
};

function invoiceCheck(data, callback){
	Invoice.findOne({'number': data.number},function (err, invoice) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (invoice) {
      result.err = false;
      result.exist = true;
      result.invoice = invoice;
      callback(result);
    } else {
      result.exist = false;
      callback(result);
    }
  });
};

function invoiceConfirm(data, callback){
  var result = new Object();
  Invoice.findById(data._id, function (err, doc) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      result.err = false;
      doc.delivered = true;
      doc.paid = true;
      doc.save();
      callback(result);
    };
  });
};

function invoiceDelete(data, callback){
  var result = new Object();
  Invoice.findByIdAndRemove(data._id, function (err) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      result.err = false;
      callback(result);
    };
  });
};

function invoiceGetAll(callback){
	Invoice.find({},function (err, invoices) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (invoices) {
      result.err = false;
      result.exist = true;
      result.invoices = invoices;
      callback(result);
    } else {
      result.exist = false;
      callback(result);
    }
  });
};

function paymentCreate(data, callback){
  var result = new Object();
  var payment = new Payment();
  payment.author = data.author;
  payment.invoice = data.invoice;
  payment.date = data.date;
  payment.summ = data.summ;
  payment.info = data.info;
  payment.ready = data.ready;
  payment.soon = data.soon;
  payment.save(function(err) {
    if (err) {
      result.err = true;
      result.cause = err;
      result.created = false;
      callback(result);
    } else {
      result.err = false;
      result.created = true;
      result.payment = data;
      callback(result);
    };
  });
};

function paymentConfirm(data, callback){
  var result = new Object();
  Payment.findById(data._id, function (err, doc) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      result.err = false;
      doc.date = data.date;
      doc.ready = true;
      doc.soon = false;
      doc.save();
      callback(result);
    };
  });
};

function paymentDelete(data, callback){
  var result = new Object();
  Payment.findByIdAndRemove(data._id, function (err) {
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    } else {
      result.err = false;
      callback(result);
    };
  });
};

function paymentGetAll(callback){
	Payment.find({},function (err, payments) {
    var result = new Object();
    if (err) {
      result.err = true;
      result.cause = err;
      callback(result);
    };
    if (payments) {
      result.err = false;
      result.exist = true;
      result.payments = payments;
      callback(result);
    } else {
      result.exist = false;
      callback(result);
    }
  });
};

app.use(express.static(__dirname + '//static'));

app.get('/', function(req, res,next) {
    res.sendFile(__dirname + '//index//index.html');
});

server.listen(27018);

io.on('connection', function (socket) {
  socket.auth = false;
  var updateAll = function(){};

	socket.on('register-submit',function(data){
		userCreate(data,function(result){
			io.sockets.connected[socket.id].emit('register-result',result);
		});
	});

	socket.on('login-submit',function(data){
		loginCheck(data,function(result){
      if(result.exist){
        socket.auth = true;
      };
			io.sockets.connected[socket.id].emit('login-result',result);
      updateAll = function(){
        paymentGetAll(function(result){
    			io.sockets.connected[socket.id].emit('payment-list',result);
    		});
        supplierGetAll(function(result){
    			io.sockets.connected[socket.id].emit('supplier-list',result);
    		});
        invoiceGetAll(function(result){
    			io.sockets.connected[socket.id].emit('invoice-list',result);
    		});
      };
		});
	});

	socket.on('supplier-submit',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		supplierCreate(data,function(result){
			io.sockets.connected[socket.id].emit('supplier-result',result);
		});
	});

	socket.on('invoice-submit',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		invoiceCreate(data,function(result){
			io.sockets.connected[socket.id].emit('invoice-result', result);
		});
	});

	socket.on('payment-submit',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		paymentCreate(data,function(result){
			io.sockets.connected[socket.id].emit('payment-result', result);
		});
	});

  socket.on('payment-confirm',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    paymentConfirm(data, function(result){
      io.sockets.connected[socket.id].emit('payment-confirm-result', result);
    });
  });

  socket.on('payment-delete',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    paymentDelete(data, function(result){
      io.sockets.connected[socket.id].emit('payment-delete-result', result);
    });
  });

  socket.on('invoice-confirm',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    invoiceConfirm(data, function(result){
      io.sockets.connected[socket.id].emit('invoice-confirm-result', result);
    });
  });

  socket.on('invoice-delete',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    invoiceDelete(data, function(result){
      io.sockets.connected[socket.id].emit('invoice-delete-result', result);
    });
  });

  socket.on('supplier-change',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    supplierChange(data, function(result){
      io.sockets.connected[socket.id].emit('supplier-change-result', result);
    });
  });

  socket.on('supplier-delete',function(data){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    supplierDelete(data, function(result){
      io.sockets.connected[socket.id].emit('supplier-delete-result', result);
    });
  });

	socket.on('supplier-getAll',function(){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		supplierGetAll(function(result){
			io.sockets.connected[socket.id].emit('supplier-list',result);
		});
	});

	socket.on('invoice-getAll',function(){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		invoiceGetAll(function(result){
			io.sockets.connected[socket.id].emit('invoice-list',result);
		});
	});

	socket.on('payment-getAll',function(){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
		paymentGetAll(function(result){
			io.sockets.connected[socket.id].emit('payment-list',result);
		});
	});

  socket.on('update-all',function(){
    if(!socket.auth){
      console.log('unauthorized');
      return;
    };
    updateAll();
  });

});
